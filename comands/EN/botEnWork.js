import { bot } from "../../index.js";
import { likeEnWork, showEnWork, urlEnWork } from '../../keyboard/EN/keyboardsEnWork.js'
import { linkToUser, linkToUserEN } from "../../keyboard/keyboards.js";

export function enWorkLogic() {
  bot.action("workEn",async (ctx) => {
    return await ctx.reply(`
Hi,my future client.👋🏻
Let's get to know each other better.

I am Artem, a web developer with more than one and a half years of experience. 
My goal is not only to create websites, but also to provide you with technologically advanced and efficient solutions. 🚀
In my work, I use the latest technologies so that your project is not only modern, but also competitive.
Ready to take responsibility for every stage of development and take into account all your needs. 💼 

For me, your success comes first. 
Let's create something spectacular together! 🚀
`,showEnWork());
});

bot.action('workProjectsEn',async(ctx)=>{
  await ctx.reply("Look at my latest works 📀", urlEnWork());
  return await ctx.reply("What do you say about my work?", likeEnWork());

})

  bot.action("likeEn", async(ctx) => {
    await ctx.reply("👍");
    await ctx.reply("I suggest you write to me in private", linkToUserEN());
    await ctx.reply(`Or you can call me 📢

+3806725180 Artem Web developer

      `);
   return await ctx.reply("I miss you and am waiting for your call 🥺");

  });
  bot.action("dislikeEn", async (ctx) => {
    await ctx.reply("👀");
    await ctx.reply(`
Please write in your private message what you did not like.
Your feedback will help me become better.
I am waiting for your message!
`,linkToUserEN());
     return await ctx.replyWithHTML(`
I will be glad to talk with you ${ctx.chat.first_name}.

+3806725180 Artem Sytnikov 

Thank you 🤝
`);});
}