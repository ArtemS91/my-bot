import { response } from 'express'
import { bot } from '../../index.js'
import { nextENSoftSkills, nextENTechSkills, returnEnIntroduction, urlEnProject } from '../../keyboard/EN/keyboardsEnHr.js'
import { purposeKeyBoardEn } from '../../keyboard/keyboards.js'
// import fs from 'fs';
import axios from 'axios'
import { URL } from 'url'
export function enHRLogic(){

  bot.action("hrEn", (ctx) => {
    ctx.replyWithHTML(
      `
Nice to meet you, <b>${ctx.chat.first_name}</b>🙂
I am an experienced Front End developer ready to join your team. 
During two years of practice in the field of web development, I learned to solve complex tasks and overcome various technical challenges.
And now I want to tell you,<b>${ctx.chat.first_name}</b> a little about myself:
          `,
          nextENTechSkills()
    );
  });

  bot.action("techSkillsEN", (ctx) => {
    ctx.reply(
      `
My level of English is Intermediate(B1)🇺🇸.

Expert in:🔴

REACT, REDUX, TANSTACK QUERY, TAILWIND, 
STYLED-COMPONENTS, AXIOX, JAVASCRIPT, SCSS, 
HTML, PIXEL PERFECT;

I have the basics:🟡

TYPESCRIPT, NEXT JS, FIREBASE, Hygraph;

Acquaintance with:🟢

NODE JS, EXPRESS, MONGO DB, Graphql`,
nextENSoftSkills()
    );
  });

bot.action("softSkillsEN", (ctx) => {
    ctx.replyWithHTML(
      `
My personal skills:🔥

  ➡️  Attentive to details; 
  ➡️  Communicative; 
  ➡️  Self-motivated;
  ➡️  I like to work in a team;
  ➡️  Adaptability;
  ➡️  Creativity.

Thank you for getting to know my skills and knowledge🔥.
I think it's time to look at my latest works:
          `,
          urlEnProject()
    );
  });
  /////https://drive.google.com/file/d/1Bun2-0njnmHLp6GaIj6eetIf0RgQdpKC/view?usp=sharing
bot.action("cvEn", async (ctx) => {
    await ctx.replyWithDocument({
      url: 'https://docs.google.com/uc?export=download&id=1ZOYyt2lJXGz3THLqSjX6eppD03LoT-jK',
      filename: "Developer_ArtemSitnikov_CV.pdf",
    });
    await ctx.reply(`Here are my contacts below⬇️

+3806725180 Artem Sytnikov 

I will wait for your call 📱.
Thank you for your time👋🏻

`);
return ctx.replyWithHTML(
      `
Want to go back to the beginning <b>${ctx.chat.first_name}</b> ?
        `,
        returnEnIntroduction()
    );
})  

  bot.action("returnEn", (ctx) => {
    ctx.reply("Welcome back 😎", purposeKeyBoardEn());
  });

}
