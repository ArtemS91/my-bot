import { bot } from "../../index.js";
import { linkToUser } from "../../keyboard/keyboards.js";
import { likeUaWork, showUaWork, urlUaWork } from "../../keyboard/UA/keyboardsUaWork.js";

export function uaWorkLogic() {
  bot.action("workUa", (ctx) => {
   return ctx.sendMessage(`
Вітаю тебе мій майбутній клієнт.👋🏻
Давай познайомимося по ближче.

Я - Артем, веб-розробник із понад півтора років досвіду. 
Моя мета - не лише створювати веб-сайти, але й надавати вам технологічно передові та ефективні рішення. 🚀
У моїй роботі я використовую найновіші технології, щоб ваш проект був не лише сучасним, але й конкурентоспроможним.
Готовий взяти на себе відповідальність за кожен етап розробки та врахувати всі ваші потреби. 💼 

Для мене ваш успіх — на першому місці. 
Давайте створимо разом щось вражаюче! 🚀
`,showUaWork());

  });


  bot.action('workProjectsUa',async(ctx)=>{
    await ctx.reply("Мої останні роботи 📀", urlUaWork());
    return await ctx.reply("Що скажеш про мою роботу ?", likeUaWork());
  })
  bot.action("likeUa", async(ctx) => {
   await ctx.reply("👍");
   await ctx.reply("Пропоную вам написати мені в особисте ", linkToUser());
   await ctx.reply(`Або ви можете набрати мене 📢
+3806725180 Артем Веб Розробник    
`);
     return await ctx.reply("Вже сумую за вами і чекаю вашого дзвінка 🥺");
  });
  bot.action("dislikeUa", async(ctx) => {
  await  ctx.reply("👀");
  await ctx.reply(
      `
Напишіть будь ласка в особисте, що вам несподобалось
Ваш зворотній звязок доможе стати мені кращим.
Чекаю вашого повідомлення!
`,
      linkToUser()
    );

    return await ctx.replyWithHTML(`
Буду радий поспілкуватися з вами ${ctx.chat.first_name}.

Ось мої контакти:➡️ +3806725180 Артем Веб Розробник
Дякую 🤝
`);
 });
}