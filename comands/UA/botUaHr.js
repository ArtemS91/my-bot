import { bot } from "../../index.js";
import {
  nextUASoftSkills,
  nextUATechSkills,
  returnUAIntroduction,
  urlUaProject,
} from "../../keyboard/UA/keyboardsUaHr.js";
import { purposeKeyBoardUA } from "../../keyboard/keyboards.js";

export default function uaHRLogic() {
  bot.action("hr", (ctx) => {
    ctx.replyWithHTML(
      `
Приємно познайомитись <b>${ctx.chat.first_name}</b>🙂
Я - досвідчений Front End розробник, готовий приєднатися до вашої команди. 
За майже два роки практики в галузі веб розробки я навчився вирішувати складні завдання та долати різні технічні виклики.
А тепер я хочу розповісти Вам,<b>${ctx.chat.first_name}</b> трохи про себе:
          `,
      nextUATechSkills()
    );
  });

  bot.action("techSkills", (ctx) => {
    ctx.reply(
      `
Володію англійською мовою на рівні Intermediate(B1) 🇺🇸.

Експерт в:🔴

REACT, REDUX, TANSTACK QUERY, TAILWIND, 
STYLED-COMPONENTS, AXIOX, JAVASCRIPT, SCSS, 
HTML, PIXEL PERFECT;

Володію основами:🟡

TYPESCRIPT, NEXT JS, FIREBASE, Hygraph;

Знайомий з:🟢

NODE JS, EXPRESS, MONGO DB, Graphql`,
      nextUASoftSkills()
    );
  });

bot.action("softSkills", (ctx) => {
    ctx.replyWithHTML(
      `
Мої особисті навички:🔥

     ➡️ Уважний до деталей; 
     ➡️ Комунікативний; 
     ➡️ Самомотивований;
     ➡️ Люблю працювати у команді;
     ➡️ Адаптивний;
     ➡️ Творчий.

Дякую, що ознайомились з моїми навичками і знаннями🔥.
Думаю прийшов час подивитися на мої останні роботи:
          `,
      urlUaProject()
    );
  });
///https://drive.google.com/file/d/14u70kQ98aXzsvmX4Fx6sOCLIfB9Ao86u/view?usp=sharing
  bot.action("cvUA", async (ctx) => {
    await ctx.replyWithDocument({
      url: 'https://docs.google.com/uc?export=download&id=14u70kQ98aXzsvmX4Fx6sOCLIfB9Ao86u',
      filename: "Девелопер__АртемСитніков_CV.pdf",
    });
    await ctx.reply(`Ось мої контакти нижче⬇️

+3806725180 Артем Ситніков 

Буду чекати вашого дзвінка 📱.
Дякую вам за приділений час 👋🏻

`);
return ctx.replyWithHTML(
      `
Хочете повернутися на початок <b>${ctx.chat.first_name}</b> ?
        `,
      returnUAIntroduction()
    );
})  

  bot.action("returnUA", (ctx) => {
    ctx.reply("З поверненням 😎", purposeKeyBoardUA());
  });
}

