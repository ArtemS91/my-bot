import pkg from 'telegraf';
const { Markup } = pkg;


export function nextEnIntroduction() {
    return Markup.inlineKeyboard([
        Markup.button.callback('We continue','nextSecond'),
    
    ], {columns:1})

}
export function nextENSoftSkills() {
    return Markup.inlineKeyboard([
        Markup.button.callback('Personal qualities 🏆 ','softSkillsEN'),
    ], {columns:1})

}

export function nextENTechSkills() {
    return Markup.inlineKeyboard([
        Markup.button.callback('Technical skills ⚙️','techSkillsEN'),
    
    ], {columns:1})
}
export function urlEnProject() {
    return Markup.inlineKeyboard([
        Markup.button.url('My own website about me','https://artem-developer-artems91.vercel.app/'),
        Markup.button.url('My GitHub','https://github.com/Artem91S'),
        Markup.button.url('My LinkedIn','https://www.linkedin.com/in/artem-sytnikov-35b55625b/'),
        Markup.button.callback('Do you want to receive a CV ?✅', 'cvEn' )
    ], {columns:1})

}

export function returnEnIntroduction() {
    return Markup.inlineKeyboard([
        Markup.button.callback('Return to the beginning ? 🔙','returnEn'),
    
    ], {columns:1})

}


