import express from "express";
import pkg from "telegraf";
const { Telegraf } = pkg;
// import dotenv from 'dotenv-safe'
import {
  getMainMenu,
  languageKeyBoard,
  linkToUser,
  purposeKeyBoardEn,
  purposeKeyBoardUA,
  removeKeyboard,
} from "./keyboard/keyboards.js";
import uaHRLogic from "./comands/UA/botUaHr.js";
import { enHRLogic } from "./comands/EN/botEnHR.js";
import { uaWorkLogic } from "./comands/UA/botUaWork.js";
import { enWorkLogic } from "./comands/EN/botEnWork.js";
const app = express();
// dotenv.config()

const webhookLink='https://my-bot-pink.vercel.app/'
const token ='6482950400:AAGDNP8i3zv0U8tCVv9LFLeUhspWcg0aosU'
export const bot = new Telegraf(token);
app.use(await bot.createWebhook({ domain:webhookLink}));
bot.start(async (ctx) => {

    await ctx.reply(
      `
Вітаю!👋🏻

Мене звати Артем і ви знаходитесь у моєму боті 🤖.
`,
removeKeyboard()
    );

    await ctx.reply(
      `Цей бот є інформаційним.
Якщо ви хочете написати мені, пишіть в особисте✍️`,
      linkToUser()
    );
    return await ctx.reply("На якій мові вам зручно спілкуватися ? ", languageKeyBoard());


});

// bot.action("nextLang",(ctx)=>{
//    ctx.reply("На якій мові вам зручно спілкуватися ? ", languageKeyBoard());
// })

bot.action("ua", (ctx) => {
  ctx.reply(
    "Дякую за відповідь🇺🇦. Тепер підскажіть будь ласка з якою метою ви тут:",
    purposeKeyBoardUA()
  );
});

bot.action("en", (ctx) => {
  ctx.reply(
    "Thanks for the answer🇺🇸. Now please tell me for what purpose you are here:",
    purposeKeyBoardEn()
  );
});

uaHRLogic();
enHRLogic();
uaWorkLogic();
enWorkLogic();
bot.on("text", (ctx) => {
  ctx.replyWithHTML(
    `
    <b>${ctx.chat.first_name}</b>,Не пишіть самостійно текст😥.
Якщо ви хочете почати, натисніть кнопку почати нижче
    `,
    getMainMenu()
  );
});

// bot.launch()

app.get("/", (req, res) => {
  res.send(`<h1>Bot work</h1>`);
});



export default app;
